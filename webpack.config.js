var path = require('path');
 
module.exports = {
    entry: './js/game.js',
    output: {
        path: path.resolve(__dirname, ''),
        filename: 'bundle.js'
    }
}