let background = {
    load : function(){
        this.y = 0;
        game.load.image('background', 'assets/background.png');
    },
    display : function(){
        this.sprites = game.add.group();
        this.sprites.position.y = this.y;
        game.add.image(0, -game.height, 'background', 0, this.sprites); 
        game.add.image(0, 0, 'background', 0, this.sprites); 
    },
    slide(speed){
        this.sprites.position.y -= speed;
        if(this.sprites.position.y<0){
            this.sprites.position.y += game.height;
        }
        this.y = this.sprites.position.y;
    },
};