let wall = {
    load : function(){
        this.y = 0;
        game.load.image('wall', 'assets/wall.png');
    },
    display : function(){
        this.sprites = game.add.group();
        this.sprites.position.y = this.y;

        game.add.sprite(0, -game.height, 'wall', 0, this.sprites); 
        game.add.sprite(0, 0, 'wall', 0, this.sprites); 
        game.add.sprite(game.width, -game.height, 'wall', 0, this.sprites).anchor.setTo(1,0); 
        game.add.sprite(game.width, 0, 'wall', 0, this.sprites).anchor.setTo(1,0); 

        this.sprites.children.forEach(wall => {
            game.physics.arcade.enable(wall);
            wall.body.immovable = true;
        });
    },
    slide(speed){
        this.sprites.position.y -= speed;
        if(this.sprites.position.y<0){
            this.sprites.position.y += game.height;
        }
        this.y = this.sprites.position.y;
    },
}
let ceiling = {
    load : function(){
        this.y = 0;
        game.load.image('ceiling', 'assets/ceiling.png');
    },
    create : function(){
        this.sprite = game.add.sprite(0, 0, 'ceiling');
        game.physics.arcade.enable(this.sprite);
        this.sprite.body.immovable = true;
    },
}