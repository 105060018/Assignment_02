let game = new Phaser.Game(500, 600, Phaser.AUTO, 'canvas');

game.global = {
    upSpeed : -170,
    nailsDamage : 4,
    score : 0,
    invincibleTime : 500,
    recordNum : 8,
};

let startState = {
    preload: function(){
        wall.load();
        ceiling.load();
        background.load();
        game.load.audio('jump',['assets/jump.ogg','assets/jump.mp3'])
        game.load.audio('down',['assets/down.ogg','assets/down.mp3'])
    },
    create: function(){
        
        game.physics.startSystem(Phaser.Physics.ARCADE);

        background.display();
        wall.display();
        let title = game.add.text(game.width/2,140,'Down the stair',{fill:'red', fontSize:45});
        title.anchor.setTo(0.5,0.5);

        let highscoreText = game.add.text(game.width/2,200,'highscore',{fill:'white', fontSize:30});
        highscoreText.anchor.setTo(0.5,0.5);
        this.updateScore();
        this.loadingText = game.add.text(game.width/2,240,'loading high score...',{fill:'white', fontSize:25});
        this.loadingText.anchor.setTo(0.5,0.5);

        let startingText = game.add.text(game.width/2,500,'press enter to start',{fill:'white', fontSize:35});
        startingText.anchor.setTo(0.5,0.5);
    },
    update: function(){
        
    },
    start: function(){
        game.state.start('play');
        game.global.score = 0;
    },
    updateScore: function(){
        server.getHighscore().then((highscores)=>{
            let i = 1;
            this.loadingText.destroy();
            highscores.forEach(record => {
                let text = i + '.' + record.name + ' : ' + record.score;
                let recordText = game.add.text(game.width/2,210 + i*30,text,{fill:'white', fontSize:20});
                recordText.anchor.setTo(0.5,0.5);
                i++;
            });
            var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            enterKey.onDown.add(this.start,this);
        });
    },
}  

let playState = {
    preload: function(){
        player.load();
        platforms.load();
    },
    create: function(){

        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.jumpSound = game.add.audio('jump');
        this.downSound = game.add.audio('down');

        background.display();
        ceiling.create();
        wall.display();
        platforms.create();
        player.create();

        this.healthText = game.add.text(30, 30,'health: 10',{fill:'red', fontSize:20});
        this.scoreText = game.add.text(game.width-30, 30,'score: 0',{fill:'red', fontSize:20});
        this.scoreText.anchor.setTo(1,0);
        this.lastScore = 0;

        // initial platforms
        platforms.add(game.width/2,game.height/2-80,'normal');
        platforms.add('random',game.height/2-10,'random');
        platforms.add('random',game.height/2+60,'random');
        platforms.add('random',game.height/2+130,'random');
        platforms.add('random',game.height/2+200,'random');
        platforms.add('random',game.height/2+270,'random');

        let randomTime = Math.random()*600+300; 
        game.time.events.add( randomTime, this.addPlatform, this);
    },
    update: function(){
        // scoll up
        background.slide(1);
        wall.slide(1.5);

        game.physics.arcade.collide(player.sprite, platforms.sprites, this.onPlatForm, null, this);
        game.physics.arcade.collide(player.sprite, wall.sprites);
        game.physics.arcade.collide(player.sprite, ceiling.sprite, this.onCeiling, null, this);

        if(!player.sprite.inWorld){
            player.sprite.kill();
        }
        if(!player.sprite.alive){
            game.state.start('gameover');
        }
        if(player.sprite.body.velocity.y>game.global.upSpeed){
            this.onAir();
        }
        platforms.move();
        player.move();
    },
    addPlatform: function(){
        platforms.add('random','bottom','random');
        let randomTime = Math.random()*600+300+game.global.score*10;
        game.time.events.add( randomTime, this.addPlatform, this);
    },
    onPlatForm:function(player, platform){
        if(platform.type==='spring'){
            this.jumpSound.play();
            platform.animations.play('jump');
            player.body.velocity.y = -350;
        }
        else if(platform.type==='nails' && this.isDamage!==1){
            this.isDamage = 1;
            game.camera.flash(0xff0000, 200);
            player.damage(game.global.nailsDamage);
            this.healthText.text = 'health: ' + player.health;
        }
        else{
            if(this.isGround!==1 && this.isCeiling!==1 && this.isDamage!==1){
                this.downSound.play();
                this.isGround = 1;
                player.heal(1);
                this.healthText.text = 'health: ' + player.health;
            }
        }
    },
    onAir: function(){
        this.isDamage = 0;
        this.isGround = 0;
        game.time.events.add( game.global.invincibleTime, ()=>{
            this.isCeiling = 0;
        },this);
    },
    onCeiling : function(player, ceiling){
        if(this.isCeiling!==1){
            this.isCeiling = 1;
            game.camera.flash(0xff0000, 200);
            player.damage(game.global.nailsDamage);
            this.healthText.text = 'health: ' + player.health;
        }
    },
}

let gameoverState = {
    preload: function(){

    },
    create: function(){
        background.display();
        wall.display();
        let title = game.add.text(game.width/2,140,'GameOver',{fill:'red', fontSize:45});
        title.anchor.setTo(0.5,0.5);

        game.add.text(game.width/2,200,'Your score: ',{fill:'white', fontSize:30}).anchor.setTo(0.5,0.5);
        
        let loadingText = game.add.text(game.width/2,250,game.global.score,{fill:'white', fontSize:50});
        loadingText.anchor.setTo(0.5,0.5);

        game.add.text(game.width/2,540,'press enter to continue',{fill:'white', fontSize:30}).anchor.setTo(0.5,0.5);
        var name = prompt("Please enter your name:", "Your name");
        if(name!==null){
            server.saveRecord(name, game.global.score);
        }
    },
    update: function(){
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start,this);
    },
    start: function(){
        game.state.start('start');
    }
};

game.state.add('start', startState);
game.state.add('play', playState);
game.state.add('gameover', gameoverState);
game.state.start('start');
