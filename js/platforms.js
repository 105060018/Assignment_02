let platforms = {
    load : function(){
        game.load.image('nails', 'assets/nails.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.spritesheet('spring', 'assets/spring.png', 97, 22);
        game.load.spritesheet('move', 'assets/move.png', 97, 22);

        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 97, 16);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 97, 16);
    },
    create : function(){
        this.sprites = game.add.group();
    },
    add : function(x, y, type){   
        if(x==='random'){
            x = Math.random() * (game.width-150) + 30;
        }
        if(type==='random'){
            let list = ['normal', 'normal', 'nails', 'spring', 'move', 'spring'];
            let n = Math.floor(Math.random()*list.length);
            type = list[n];
        }
        if(y==='bottom'){
            y = game.height;
        }

        let newPlatform = this.sprites.getFirstDead();
        if(!newPlatform){
            newPlatform = game.add.sprite(x, y, type, 0,this.sprites);
            newPlatform.type = type;
        }
        else{
            let list = ['normal', 'normal', 'nails', 'spring', 'move', 'spring'];
            let n = Math.floor(Math.random()*list.length);
            type = list[n];
            newPlatform.reset(x, y);
            newPlatform.loadTexture(type);
            newPlatform.type = type;
            game.global.score++;
            playState.scoreText;
            playState.scoreText.text = 'score: ' + game.global.score;
        }
        newPlatform.checkWorldBounds = true;
        newPlatform.outOfBoundsKill = true;

        game.physics.arcade.enable(newPlatform);
        if(type==='nails'){
            newPlatform.body.setSize(96, 15, 0, 15);
        }
        else{
            newPlatform.body.setSize(96, 22, 0, 0);
        }
        if(type==='spring'){
            newPlatform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        }
        if(type==='move'){
            newPlatform.body.velocity.x = 80;
        }

        newPlatform.body.velocity.y = game.global.upSpeed;
        newPlatform.body.immovable = true;
        newPlatform.body.checkCollision.down = false;
        newPlatform.body.checkCollision.right = false;
        newPlatform.body.checkCollision.left = false;
    },
    move: function(){
        this.sprites.children.forEach(platform=>{
            if(platform.type==='move'){
                if(platform.position.x>game.width-170){
                    platform.body.velocity.x = -80;
                }
                else if(platform.position.x<30){
                    platform.body.velocity.x = 80;
                }
            }
        });
    },
};