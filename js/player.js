let player = {
    load : function(){
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        this.cursor = game.input.keyboard.createCursorKeys();
    },
    create : function(){
        // animate
        this.sprite = game.add.sprite(game.width/2, game.height/2 - 200, 'player');
        this.sprite.animations.add('left', [0, 1, 2, 3], 15);
        this.sprite.animations.add('right', [9, 10, 11, 12], 15);
        this.sprite.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.sprite.animations.add('flyright', [27, 28, 29, 30], 12);
        this.sprite.animations.add('fly', [36, 37, 38, 39], 12);
        
        this.sprite.maxHealth = 10;
        this.sprite.health = 10;
        this.sprite.checkWorldBounds = true;
        // physic setting
        game.physics.arcade.enable(this.sprite);
        this.sprite.body.gravity.y = 500;
    },
    move : function(){
        if (this.cursor.left.isDown) {
            this.sprite.body.velocity.x = -200;
        }
        else if (this.cursor.right.isDown) {
            this.sprite.body.velocity.x = 200;
        }
        else{
            this.sprite.body.velocity.x = 0;
        }

        let x = this.sprite.body.velocity.x;
        let y = this.sprite.body.velocity.y;
        if (x < 0 && y > game.global.upSpeed) {
            this.sprite.animations.play('flyleft');
        }
        if (x > 0 && y > game.global.upSpeed) {
            this.sprite.animations.play('flyright');
        }
        if (x < 0 && y == game.global.upSpeed) {
            this.sprite.animations.play('left');
        }
        if (x > 0 && y == game.global.upSpeed) {
            this.sprite.animations.play('right');
        }
        if (x == 0 && y != game.global.upSpeed) {
            this.sprite.animations.play('fly');
        }
        if (x == 0 && y == game.global.upSpeed) {
          this.sprite.frame = 8;
        }
    },
}